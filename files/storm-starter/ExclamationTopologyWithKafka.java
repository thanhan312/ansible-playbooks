package storm.starter;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.testing.TestWordSpout;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import backtype.storm.utils.Utils;
import backtype.storm.spout.SchemeAsMultiScheme;
import storm.kafka.*;

import java.util.*;

/**
 * This is a basic example of a Storm topology.
 */
public class ExclamationTopologyWithKafka {

  public static class ExclamationBolt extends BaseRichBolt {
    OutputCollector _collector;

    @Override
    public void prepare(Map conf, TopologyContext context, OutputCollector collector) {
      _collector = collector;
    }

    @Override
    public void execute(Tuple tuple) {
      _collector.emit(tuple, new Values(tuple.getString(0) + "!!!"));
      _collector.ack(tuple);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
      declarer.declare(new Fields("word"));
    }


  }

  public static void main(String[] args) throws Exception {
    TopologyBuilder builder = new TopologyBuilder();

    List<String> hosts = new ArrayList<String>();
    hosts.add("kafka-01"); 
    hosts.add("kafka-02"); 

    List<String> zkServers = new ArrayList<String>();
    zkServers.add("storm-01"); 
    zkServers.add("storm-02"); 
    zkServers.add("storm-03"); 

    SpoutConfig spoutConfig = new SpoutConfig(
      SpoutConfig.StaticHosts.fromHostString(hosts, 1),
      "test", // topic to read from
      "/kafkastorm", // the root path in Zookeeper for the spout to store the consumer offsets
      "discovery"); // an id for this consumer for storing the consumer offsets in Zookeeper
    spoutConfig.scheme = new SchemeAsMultiScheme(new StringScheme());
    spoutConfig.zkServers = zkServers;
    spoutConfig.zkPort = 2181;
    KafkaSpout kafkaSpout = new KafkaSpout(spoutConfig);

    builder.setSpout("kafka", kafkaSpout);
    builder.setBolt("exclaim1", new ExclamationBolt(), 3).shuffleGrouping("kafka");
    builder.setBolt("exclaim2", new ExclamationBolt(), 2).shuffleGrouping("exclaim1");

    Config conf = new Config();
    conf.setDebug(true);

    if (args != null && args.length > 0) {
      conf.setNumWorkers(3);

      StormSubmitter.submitTopology(args[0], conf, builder.createTopology());
    }
    else {

      LocalCluster cluster = new LocalCluster();
      cluster.submitTopology("test", conf, builder.createTopology());
      Utils.sleep(10000);
      cluster.killTopology("test");
      cluster.shutdown();
    }
  }
}
