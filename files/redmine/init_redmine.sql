create database redmine default character set utf8;
grant all on redmine.* to user_redmine identified by '{{ redmine_db_password }}';
flush privileges;
