%define spark_base spark_standalone
%define spark_name spark
%define spark_branch 0.8.0
%define spark_version 0.8.0
%define spark_release incubating
%define spark_ver %{spark_version}-%{spark_release}
%define spark_home /var/lib/%{spark_base}/%{spark_name}-%{spark_version}
%define spark_config %{spark_home}/conf
%define spark_user spark
%define spark_group spark

Name: %{spark_name}
Version: %{spark_version}
Release: %{spark_release}
Summary: Apache Spark
Group: Applications/Internet
License: Apache
URL: http://spark.incubator.apache.org/
Source: spark-0.8.0.tar.gz
BuildRoot: %(mktemp -ud %{_tmppath}/%{spark_name}-%{spark_version}-%{spark_release}-XXXXXX)
%description
Apache Spark is an open source cluster computing system that aims to make data analytics fast — both fast to run and fast to write.

To run programs faster, Spark offers a general execution model that can optimize arbitrary operator graphs, and supports in-memory computing, which lets it query data faster than disk-based engines like Hadoop.

To make programming faster, Spark provides clean, concise APIs in Scala, Java and Python. You can also use Spark interactively from the Scala and Python shells to rapidly query big datasets.


%prep
%setup -q

# This SPEC build is Only Packaging.
%build

%install
# Clean out any previous builds not on slash (lol)
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

# Copy the spark_standalone file to the right places
%{__mkdir_p} %{buildroot}/var/lib/%{spark_base}/%{spark_name}-%{spark_version}
%{__cp} -R * %{buildroot}/var/lib/%{spark_base}/%{spark_name}-%{spark_version}/
%{__ln_s} /var/lib/%{spark_base}/%{spark_name}-%{spark_version} %{buildroot}/var/lib/%{spark_base}/default

# Form a list of files for the files directive
echo $(cd %{buildroot} && find . | cut -c 2-) | tr ' ' '\n' > files.txt

%clean
[ "%{buildroot}" != "/" ] && %{__rm} -rf %{buildroot}

%pre
getent group %{spark_group} >/dev/null || groupadd -r %{spark_group}
getent passwd %{spark_user} >/dev/null || /usr/sbin/useradd --comment "Storm Daemon User" --shell /bin/bash -M -r -g %{spark_group} --home /var/lib/spark_standalone/default %{spark_user}

%files -f files.txt
%defattr(-,%{spark_user},%{spark_group},-)
