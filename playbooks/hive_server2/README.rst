##########
README
##########

* first_const.yml ... A playbook to be used for the first construction
                      This playbook includes initialization of metastore

* non_first_cont.yml ... A playbook to be used for NOT first construction

.. vim: ft=rst tw=0
